Présentation atelier « Loi(s) renseignement - SysInfo RESIC 2016
================================================================================

<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
# Git : édition et partage de la présentation

## Workflow

Le principe :
* Vous arrivez chez vous, vous voulez avancer sur les slides. Vous commencez par
  récupérer la dernière version du projet présente sur le serveur. À l’issue de
  cette commande, ill va peut être falloir résoudre des « conflits », si jamais
  des modifications ont été faites sur des zones que vous avez aussi modifiées
  la dernière fois.  De plus, il est nécessaire de « commiter » avant le faire
  le pull. La commande est la suivante (à lancer, comme toutes les commandes
  git, depuis le dossier du projet) :

```
git pull
```

* Vous faites ensuite vos modifications, dans notre cas ce sera principalement
  sur le fichier presentation.md.
* Lorsque vous avez fait un certain nombre de modifications (par exemple rédigé
  une partie), il faut « commiter », ie enregistrer les modifications avec un
  message :

```
git commit -a -m "Rédaction de la partie II.1."
```

* Vous pouvez continuer à travailler, faire d’autres commits, mais toutes ces
  modifications ne sont pour l’instant faites que sur votre poste local.
* Lorsque vous avez fini et que vous voulez envoyer toutes vos modifications
  (tous vos commits) sur le serveur, il faut « pusher » le tout. Attention, il
  est nécessaire que toutes les modifications aient été commitées avant le push
  (ie pas de modification des fichiers après le dernier commit). La commande :

```
git push
```

* Et ainsi de suite ! Moralité :
  - bien penser à faire un pull lorsqu’on recommence à travailler
  - faire des commit réguliers
  - faire un push lorsqu’on a terminé

## Installation

* D’abord, installer git
* S’inscrire sur git.framasoft.org (perso j’ai mis mon nom, mais vous pouvez
  mettre un pseudo), et envoyez-moi votre login pour que je vous ajoute au
  projet et que vous puissiez contribuer
* Dans le dossier que vous voulez (un nouveau dossier avec le projet sera créé),
  faites :

```
git clone git@git.framasoft.org:bnarang/loi-renseignement-resic2016.git
cd loi-renseignement-resic2016
```

* Vous êtes prêts =) Il faudra que je vous ajoute aux contributeurs pour que
  vous puissiez pusher des modifs !



<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
# Pandoc : conversion en html / js

## Compilation et affichage

Pour convertir le fichier presentation.md en html, taper simplement :
```
make
```

dans le répertoire du projet. Cela va générer le fichier
`output/index.html`, que vous pouvez ouvrir dans votre navigateur
favoris, ça devrait donner quelque chose de joli sinon il y a eu un problème ;)

## Syntaxe

Il faut respecter certaines règles simples pour que ça donne ce qu’on veut :

* Une slides est délimitée par un `##`. S’il est suivi d’un texte, celui-ci
  s’affichera en titre : `## Mon titre de slide`.
* Des slides peuvent être « regroupées » (déplacement vertical). Un groupe de
  slide est délimité par un `#`. Ne pas mettre de texte après, sinon celui-ci
  s’affiche sur toutes les slides du groupe…
* En général, plutôt commencer un groupe de slide avec un titre `##`, et chaque
  slide avec un titre `###` :

```
#

## Mon groupe

### Slide 1

hello

### Slide 2

there
```

* Les listes à puces se font ainsi :

```
* un
* deux
```

* Pour mettre une citation :

```
> Il faut beau.
```

* Pour faire en sorte qu’un objet ne s’affiche pas tout de suite mais à l’action
  suivante (clic ou flèche), le mettre entre des balises ainsi :

```
* un
<div class="fragment">
* deux
</div>
<div class="fragment">
* trois
</div>
```

* Pour insérer une image : `!()[images/monimage.png]`. L’image doit être placée
  dans le dossier `output/images`.
* Pour ajouter un peu d’air, on peut insérer manuellement des espaces verticaux
  avec `<br>`.


## Exporter en pdf

Il est possible d’exporter le rendu reveal.js en pdf. Pour ça
* ouvrir la présentation html dans Chrome/Chromium avec ?print-pdf à la fin
* ouvrir le menu d’impression, choisir « Enregistrer au format pdf », et choisir
  « Aucune marge », puis enregistrer



<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
# La présentation pour les RESIC / TODO

On s’occupe ici de ce qui sera projeté, donc il faut se limiter à l’essentiel.
Privilégier des listes à puces, peu de texte par slides, une idée par slide, une
image ça peut être bien.

Surtout, bien noter les références ; je les compilerai à la fin, ça sera l’objet
des dernières slides, et on pourra les imprimer ou les fournir à côté.

Bon courage :)

