# Make all => compile en reveal.js
all: presentation.md
	pandoc -t html5 --template=./output/template-revealjs.html --standalone --section-divs presentation.md -o output/index.html

#pandoc presentation.md -t beamer --filter pandoc-citeproc  --bibliography=reference.bib -V theme:Warsaw --slide-level=2 -o presentaion.pdf
