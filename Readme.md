Présentation atelier « Loi(s) renseignement - SysInfo - RESIC 2016
================================================================================

La loi relative au renseignement, promulguée en 2015, s’inscrit dans une suite de mesures destinées à lutter contre des menaces particulières visant la France. Connaissez-vous les détails de ce texte ?
Avons-nous à trouver un compromis entre libertés individuelles et sécurité ? Et quelles sont les dérives potentielles ? Nous proposons d'étudier les motifs et arguments utilisés pour faire passer ces lois,
puis d'examiner les risques associés à cette nouvelle législation pour les citoyens et la société.

*Dimanche 13 mars 2016 9H-10H30 Salle 4*
