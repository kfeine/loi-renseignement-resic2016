% Loi renseignement : sécurité contre liberté ?
% Sysinfo
% RESIC 2016


<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Partie I : loi renseignement                                            -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
# partie I

---

## Loi renseignement : état des lieux

---

![](images/eye-cctv-rond.png)


<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Groupe slides qui a dit quoi                                            -->
#

## Qui a dit quoi ?

##

### Les propositions

<br>

* Jean Marie Le Pen
* Général de Gaule
* François Mitterrand
* Manuel Valls
* Jean-Pierre Raffarin
* Nicolas Sarkozy
* Dominique de Villepin
* George Orwell
* Alain Peyrefitte (Garde des Sceaux sous VGE)

##

### Qu. 1

<br>

> Il n'y a aucune contradiction à vouloir renforcer à la fois la sécurité et la
> liberté. **La sécurité sans la liberté c'est l'oppression ; la liberté sans la
> sécurité, c'est la jungle !**

<br>

<div class="fragment">
— Alain Peyrefitte, 1981
</div>

##

### Qu. 2

<br>

> La sécurité… première des libertés.

<br>

<div class="fragment">
– Jean Marie Le Pen, 1992, campagne régionales PACA
</div>

##

![](images/JMLP.jpg)

##

### Qu. 3

<br>

> Sans sécurité il n'y a pas de liberté.

<br>

<div class="fragment">
— Nicolas Sarkozy, 2002
</div>

##

### Qu. 4

<br>

> La sécurité est la première des libertés, c'est pourquoi d'autres libertés
> pourront être limitées.

<br>

<div class="fragment">
— Manuel Valls, 2015
</div>

##

### Mais aussi…

<br>

* Lionel Jospin, 1997
* Daniel Vaillant, 2001
* Jaques Chirac
* Christian Estrosi, 2015
* Et d'autres ;)



<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Groupe slides détail de la loi                                          -->
#

## Petit résumé des dangers de la loi renseignement

<br>

![](images/RienACacher.jpg)

##

### LOI n° 2015-912 du 24 juillet 2015 relative au renseignement

* Assemblée : 438 votes pour, 42 abstentions et 86 votes contre
* Sénat : 252 voix pour, 67 voix contre et 26 abstentions

##

### Les finalités

<br>

1. Indépendance et **défense** nationale, intégrité du territoire
2. Intérêts majeurs de la **politique étrangère**
3. Intérêts **économiques** industriels et scientifiques majeurs
4. **Terrorisme**
5. Forme républicaine des **institutions**, sécurité nationale
6. **Criminalité** et délinquance organisées
7. Prolifération des **armes de destruction massive**

##

### Indépendance nationale

<br>

- Les zenvahisseurs
- Les indépendantistes
- Les séparatistes
- Les anarchistes, autonomistes, zadistes
- Les contractuels de la défense nationale
 <div class="fragment">
 	- Et leurs sous-traitants
 	- Et leurs clients
 	- Et leurs fournisseurs
 </div>

##

### Intérêts politiques et économiques

<br>

* Contre espionnage
* Relations avec les gouvernements étrangers
* Commerce international
* Intelligence économique défensive et offensive.

<br>

<div class="fragment">
Ce n'est pas sans rappeler ce que fait la NSA !
</div>

##

### Terrorisme ? Définition

<br>

> Ensemble des actes de violence qu'une organisation politique exécute dans le
> but de désorganiser la société existante et de créer un climat d'insécurité
> tel que la prise du pouvoir soit possible

— CNRTL

##

### Terrorisme ? Dans l'histoire

<br>

<div class="fragment">
* La doctrine des partisans de la terreur (1789)
</div>
<div class="fragment">
* La stratégie de la tension en Italie à partir de 1964
</div>
<div class="fragment">
* Tarnac…
</div>
<div class="fragment">
* Attentats
</div>

##

### Atteintes aux institutions et sécurité nationale

<br>

<div class="fragment">
* Renverser le gouvernement
* Casseurs organisés
* Groupes d'extrême droites
* …
</div>

<br>

<div class="fragment">
* Syndicalistes
* Nouveaux partis politiques
* Meneurs de grèves
* …
</div>

##

### Criminalité et délinquance organisées

<br>

* Jeux d'argent
* Police aux frontières
* Ferroviaire <!-- pas clair : qu'est-ce que c'est ? lien avec le crime organisé
-->
* Passeurs et autres personnes portant assistance aux étrangers en situation
  irrégulière

##

### Prolifération des armes de destruction massive

<br>

* **B**actériologique
* **N**ucléaire
* **C**himique

##

### Services ayant accès au dispositif

* La direction de la sécurité extérieure
* La direction de la sécurité intérieure

<div class="fragment">
* La sous-direction des affaires économiques et financières
* Le service central des courses et jeux
</div>

<div class="fragment">
* Le service « traitement du renseignement et action contre les circuits
  financiers clandestins »
* L’office central pour la répression de l'immigration irrégulière et de
  l'emploi d'étrangers sans titre de la direction centrale de la police aux
  frontières
</div>

<div class="fragment">
* L’unité judiciaire du service national de la police ferroviaire
</div>

<div class="fragment">
* Au total une cinquantaine de (sous-)services
</div>

##

### Traduisons-les

<br>

Quand l'État propose de protéger les citoyens du terrorisme,

il se donne en fait les moyens de protéger **ses** intérêts.

##

### La CNCTR

<br>

* Commission de Contrôle des Techniques de Renseignement
* 9 personnes
* Formule un avis auprès du premier ministre avant la mise en œuvre de toute
  surveillance

<div class="fragment">
[hors cas d'urgence]
</div>

<div class="fragment">
* Possibilité de lui demander si on est sous surveillance
</div>

<br>

<div class="fragment">
Ressemble encore à la cour FISA pour la NSA…
</div>

##

### Les boîtes noires

<br>

Le Premier ministre pourra imposer à tous les acteurs des nouvelles technologies
une « boîte noire » sur leurs infrastructures « destinée à révéler, sur la seule
base de traitements automatisés d’éléments anonymes, une menace terroriste ».

<br>

<div class="fragment">
Personne ne sait comment ça fonctionnera.
</div>

##

### Les métadonnées, définition

<br>

> Les métadonnées se composent de tout ce qui soit relative aux informations,
> sous réserve de l’information en elle-même. Le contenu d’un message n’est pas
> une métadonnée, mais son expéditeur, la date de son envoi, le lieu et on
> destinataire sont tous des exemples de métadonnées.

— Electronic Frontier Foundation

<br>

La collecte de ces métadonnées ne serait pas intrusive dans notre vie privée…

##

### Les métadonnées, ce qu'elles disent

<br>

* En disent plus que le contenu du message
* Ex : votre travail, vos opinions, vos relations, vos amant(e)s, votre santé…
* Bref : les métadonnées permettent de savoir qui vous êtes, ce que vous faites…

##

### Les valises IMSI

<br>

Dispositif de la taille d'une grosse valise permettant de lister tous les
téléphones portables présents dans la zone, voire d'enregistrer les flux.



<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Groupe slides efficacité                                                -->
#

## Efficace ?

##

### L'exemple NSA

<br>

> We know of at least 50 threats that have been averted

— Barack Obama et Keith Alexander (ex-directeur de la NSA)…

<br>

<div class="fragment">
En réalité seulement 13 aux U.S. entre 2002 et 2013, avec une surveillance massive
de tous les échanges !
</div>

##

### En France

<br>

> La quincaillerie de Prism ne remplace pas le renseignement humain.

— Alain Bauer



<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- questions                                                               -->

#

## Questions ?





<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Partie II : déconstruction de la peur                                   -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
# partie II

---

## De quoi avez-vous peur ?

---

![](images/banksy.jpg)

<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Groupe slides quizz morts                                               -->
#

## Quizz mortel

![](images/mort.jpg)

##

### Règles

<br>

Classer les causes de mortalité en France (moyenne entre 2000 et 2016)

* Suicide
* Noyade
* Tuberculose
* Attentat terroriste
* Cancer
* Accident de la route
* Accident d’avion

##

### Résultats

<br>

| **Cause** | **Nombre de morts** |
|----------------------|:------:|
| Cancer               | 147500 |
| Suicide              | 12900  |
| Accident de la route | 4800   |
| Tuberculose          | 500    |
| Noyade               | 400    |
| Attentat             | 14     |
| Accident d’avion     | 8      |


<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Groupe slides sur la peur du terrorisme                                 -->
#

## Peur de quoi ?

![](images/chat-kamikaze.jpg)

##

### La véritable menace terroriste

<br>

Quelques chiffres :

<!-- À vérifier ! -->
* 32 victimes du terrorisme de 2000 à février 2015
* Et pourtant…

![](images/vigipirate.png)

##

### Les attentas en France depuis 1960

![](images/Frise.png)

##

### Le terrorisme représente-t-il un risque pour nos libertés ?

<!-- ici le but est de faire comprendre que le niveau actuel de sécurité en
France nous permet largement de jouir de nos libertés (ce qui n'est pas le cas
dans tous les pays) -->

<br>

Par peur du terrorisme vous :

* Allez moins faire vos courses
* Rencontrez plus de difficultés à exposer vos opinions
* Avez dû modifier vos habitudes vestimentaires
* Avez dû déménager
* Ne consultez plus les mêmes sources d'information qu'avant

##

### Nous luttons déjà contre le terrorisme

<br>

* Spécialistes : surveillance ciblée > surveillance de masse
* Exemple de l'efficacité et exemplarité de la NSA

##

### Quelle(s) sécurité(s) ?

<br>

DDHC de 1789 : la sûreté est un des 4 droits naturels et imprescriptibles de
l'homme

C'est ce qui protège le **citoyen** de **l'arbitraire** de l'état.

<br>

<div class="fragment">
> Si équilibre il doit y avoir, c’est **entre la puissance de l’État et la
> liberté individuelle**, et non pas entre la sécurité et la liberté des
> individus.

— LMSI
</div>

##

### Et selon le gouvernement

<br>

> Aujourd’hui, la notion de sûreté prend un nouveau sens. Il s’agit de mieux
> assurer la sécurité des citoyens au quotidien.

— viepublique.fr


##

### Conclusion

<br>

**Aujourd'hui, nos libertés individuelles sont-elles d'avantage menacées par les
mesures liberticides ou par le terrorisme ?**

<br>

<div class="fragment">
> Il ne s'agit pas d'opposer liberté et sécurité, au risque de perdre les deux.

— Daniel Muraz dans *Le Courrier Picard*
</div>





<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Partie III : rien à cacher ?                                            -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
# partie III

---

## Rien à cacher ?

![](images/lock.jpg)

---

<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Groupe slides tous surveillés                                           -->
#

## Tous surveillés !

##

### Règles

<br>

Déplacez vous si vous avez…

<br>

<div class="fragment">
* Commis un attentat sur le territoire français
</div>
<div class="fragment">
* Travaillé (ou postulé) en relation avec la défense nationale
</div>
<div class="fragment">
* Fabriqué des armes de destruction massive
</div>
<div class="fragment">
* Discuté politique à l'étranger
</div>
<div class="fragment">
* Voyagé en Syrie, Irak, Afghanistan, Mali, Burkina Faso, Libye...
</div>
<div class="fragment">
* Communiqué avec un de ces pays ou un de leurs ressortissants
</div>


##

### Règles (suite)

<br>

<div class="fragment">
* Consulté des sites traitant d'islamisme ou Moyen Orient (information
  alternative)
</div>
<div class="fragment">
* Participé à un mouvement « violent » contre une mesure du gouvernement
  (mouvements étudiants, ZAD, …)
</div>
<div class="fragment">
* Utilisé un moyen de communication chiffré pour les SMS, les emails, l'Internet
</div>
<div class="fragment">
* Été lié au grand banditisme, trafic de stupéfiants
</div>

##

### Règles (encore)

<br>

<div class="fragment">
* Dans votre entourage, une personne dans l’une des situations précédentes
</div>
<div class="fragment">
* Et dans l'entourage de votre entourage
</div>


<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Groupe slides ce qu’on a à cacher                                       -->
#

## Rien à cacher, vraiment ?

##

### Un argument bien connu

<br>

> S’il y a quelque chose que vous faites et que personne ne doit savoir,
> peut-être qu’il faudrait ne pas le faire en premier lieu.

— Eric Schmidt, ex-PDG de Google

<br>

<div class="fragment">
Suppose que :

1. Tout ce qui n'est pas mal est publiable
2. Le surveillant possède la bonne norme de bien
3. Cela ne changera pas
</div>

##

### Ce que vous n'avez surtout pas à cacher

<br>

* À votre patron que vous chercher un travail
* Au gouvernement que vous préparez une action contre la loi travail
* À vos voisins votre vie sexuelle
* À votre assureur une maladie grave
* À tout le monde quels sites vous visitez, les SMS que vous envoyez

<br>

*Si vous ne faites rien de mal, vous ne verrez pas d'objection à l'installation
de caméras dans votre chambre pour lutter contre les violences domestiques ?*


##

### N'aurions-nous rien à cacher ?

<br>

* Certaines de nos opinions
* Certains de nos engagement
* Certaines de nos relations
* Certaines de nos conversations
* Certains de vos comportements
* Certains de nos achats
* Ceux de nos proches

##

### À qui ?

<br>

* Notre entourage
* Le reste du monde
* L'État
* Des entreprises privées
* …

##

### D'autant plus pour certains

<br>

* Journalistes
* Avocats
* Médecins
* Politiques
* …

##

### L'avis des concernés

<br>

> Les lois successives sur la sécurité, le renseignement et l’anti-terrorisme
> qui n’ont […] pas permis d’éviter ces attaques […] sont inquiétantes en ce
> qu’elles accentuent encore la dérive vers la constitution d’un État policier.
> Elles sont une menace pour les libertés fondamentales.

— Syndicat des avocats de France.

##

### Une question collective

<br>

* C'est une chose de renoncer à sa vie privée
* C'en est une autre de l'imposer à son voisin


<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Groupe slides le danger sociétal de la surveillance de masse            -->
#

## Surveillance globale et comportement individuel

##

### Le panoptique

![](images/panoptique.jpg)

##

### Implications

<br>

Quelles conséquences d'une surveillance sur...

* L'investissement politique, associatif, religieux
* La diversité politique, d’opinions
* Les journalistes, avocats…
* Et les citoyens dans leurs actes et questionnements « subversifs »

##

### Impact sur la société

<br>

Une société sous surveillance, c'est une société qui va globalement se conformer
aux opinions dictées par ceux qui la gouvernent.

##

### Une question de confiance

<br>

> On peut avoir le gouvernement le plus responsable du monde aujourd'hui. Mais,
> demain, cela peut changer.

— Edward Snowden

<br>

<div class="fragment">
> En l'état actuel du texte la France peut basculer dans la dictature en une
> semaine.

— Frédéric Sicard, bâtonnier de Paris
</div>

##

### Une note d'optimisme ?

<br>

> Mais au siècle dernier, une idée forte était celle de s’approprier les moyens
> de production. Je pense que nous devons nous approprier nos moyens de
> communication. (…) Le président des États-Unis nous dit que nous sommes face
> à un équilibre à trouver entre vie privée et sécurité. C’est faux. Nous
> pouvons avoir les deux, mais uniquement si nous avons les deux : il n’y a pas
> de sécurité sans droit à la vie privée.

— Edward Snowden





<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
<!-- Partie IV : fin                                                         -->
<!-- +++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++ -->
#

## Annexes

---

##

### Références

<br>

* *Nulle part où se cacher*, Glenn Greenwald, 2014 (livre)
* *Citizenfour*, Laura Poitras, 2014 (film)
* [legifrance.gouv.fr](https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000030931899&categorieLien=id)
* [La quadrature du net](https://lqdn.fr)
* [NextInpact](www.nextinpact.com)
* [Numerama](www.numerama.com)
* [Je n'ai rien à cacher](jenairienacacher.fr)
* [Surveillance Self-Defense par l'EFF](https://ssd.eff.org/fr)
* *The Circle*, Dave Eggers, 2013 (roman)

##

### Crédits des images

<br>

* Slide partie I par duncan c [sur Flickr](https://www.flickr.com/photos/duncan/5510161001/)
* Slide résumé loi par [Télérama](http://www.telerama.fr/medias/big-brother-ou-big-bazar-le-projet-de-loi-de-programmation-militaire-fait-controverse,106215.php)
* Slide partie II de Banksy par Kim Davies [sur Flickr](https://www.flickr.com/photos/kjd/2044462261/)
* Slide quizz mortalité *Terry Pratchett's Death and the Death of Rats* par Paul Kidby
* Slide rien à cacher par cocoparisienne [sur pixabay](https://pixabay.com/fr/serrure-de-porte-château-clé-407427/)
* Slide panoptique, à Cuba, par Friman [sur Wikimedia](https://commons.wikimedia.org/wiki/File:Presidio-modelo2.JPG)

##

### Cette présentation

<br>

![](images/urlpres.png)

[https://git.framasoft.org/bnarang/loi-renseignement-resic2016](https://git.framasoft.org/bnarang/loi-renseignement-resic2016)



